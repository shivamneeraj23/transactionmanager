from django.urls import path
from .views import health_check, TransactionManagerView, CreditManagerView

urlpatterns = [
    path('health/', health_check, name='ping'),
    path('transfer/', TransactionManagerView.as_view()),
    path('credit-self/',CreditManagerView.as_view())
]