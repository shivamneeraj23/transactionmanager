from bank.models import UserAccount
from django.contrib.auth.models import User

from bank.transaction_helper.credit_manager import CreditService
import random
import string


def randStr(chars=string.ascii_lowercase + string.digits, N=10):
    return ''.join(random.choice(chars) for _ in range(N))


def generate_dummy_data():
    # create Bank Accounts
    user1 = User.objects.create(username=randStr(N=5))
    bo1 = UserAccount.objects.create(
        user=user1,
        is_active=True,
    )
    bo1.save()

    user2 = User.objects.create(username=randStr(N=5))
    bo2 = UserAccount.objects.create(
        user=user2,
        is_active=True,
    )
    bo2.save()

    # Credit amount to created bank accounts
    bo1 = UserAccount.objects.get(user=user1)
    bo2 = UserAccount.objects.get(user=user2)
    cs1 = CreditService(account_no=bo1.account_no, amount=10000)
    cs1.perform_credit()
    cs2 = CreditService(account_no=bo2.account_no, amount=10000)
    cs2.perform_credit()
