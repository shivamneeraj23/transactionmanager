import uuid
from datetime import datetime

import redis
import os
from django.db import transaction

from bank.models import UserAccount, Transactions
from bank.transaction_helper.bank_conf import TransactAPIErrorCode, DEFAULT_TRANSACTION_API_TTL
from bank.transaction_helper.custom_exceptions import InvalidRequestException, InsufficientBalanceException

redis_client = redis.Redis(host='redis',
                           port=os.getenv('REDIS_PORT'),
                           db=os.getenv('REDIS_DB'))


class TransactService:
    def __init__(self, credit_account_no, debit_account_no, amount, default_request_ttl=DEFAULT_TRANSACTION_API_TTL):
        """
        This is the TransactService Layer which handles the transactions done on the system
        :param credit_account_no:
        :param debit_account_no:
        :param amount: The Amount to be transffered
        :param default_request_ttl(unit seconds): This is used to rate limit on the number of request a user can make
        with the same payload
        :return:
        """
        self.__validate_request_body(credit_account_no, debit_account_no, amount)
        self.credit_account_no = credit_account_no
        self.debit_account_no = debit_account_no
        self.amount = float(amount)
        self.default_request_ttl = default_request_ttl
        self.validate_and_put_in_redis()

    def get_redis_key(self):
        return f'{self.credit_account_no}_{self.debit_account_no}_{self.amount}'

    def validate_and_put_in_redis(self):
        if redis_client.get(self.get_redis_key()):
            raise InvalidRequestException(TransactAPIErrorCode.MULTIPLE_TRY_IN_SHORT_INTERVAL.value)
        else:
            redis_client.set(self.get_redis_key(), 1, self.default_request_ttl)


    def __validate_request_body(self, credit_account_no, debit_account_no, amount):
        try:
            if not (credit_account_no and debit_account_no and amount):
                raise InvalidRequestException(TransactAPIErrorCode.INSUFFICIENT_INFO.value)
            if not UserAccount.objects.filter(account_no=credit_account_no).exists():
                raise InvalidRequestException(TransactAPIErrorCode.INVALID_CREDITOR_ACCOUNT.value)
            if not UserAccount.objects.filter(account_no=debit_account_no).exists():
                raise InvalidRequestException(TransactAPIErrorCode.INVALID_DEBITOR_ACCOUNT.value)
            if float(amount) <= 0:
                raise InvalidRequestException(TransactAPIErrorCode.NEGATIVE_AMOUNT.value)
        except InvalidRequestException as err:
            raise err
        except Exception:
            raise InvalidRequestException(TransactAPIErrorCode.INVALID_REQUEST.value)

    @staticmethod
    def get_transaction_id():
        return str(uuid.uuid4())

    def perform_transaction(self):
        try:
            resp = {}
            # transaction.atomic() block guarantees atomicity of operations performed here.
            with transaction.atomic():
                transaction_id = TransactService.get_transaction_id()
                transaction_time = datetime.now()
                # select_for_update - implements DB Level Lock on the row until the transaction is over.
                creditor_acc_obj = UserAccount.objects.select_for_update().get(account_no=self.credit_account_no)
                debitor_acc_obj = UserAccount.objects.select_for_update().get(account_no=self.debit_account_no)
                if self.amount > debitor_acc_obj.account_bal:
                    raise InsufficientBalanceException(TransactAPIErrorCode.INSUFFIENT_BALANCE.value)
                Transactions.objects.create(
                    txn_type=Transactions.TXN_TYPE_CHOICES[1][0],
                    txn_id=transaction_id,
                    txn_amount=self.amount,
                    txn_acc=debitor_acc_obj,
                    txn_time=transaction_time
                )
                Transactions.objects.create(
                    txn_type=Transactions.TXN_TYPE_CHOICES[0][0],
                    txn_id=transaction_id,
                    txn_amount=self.amount,
                    txn_acc=creditor_acc_obj,
                    txn_time=transaction_time
                )
                creditor_acc_obj.account_bal += self.amount
                creditor_acc_obj.save()
                debitor_acc_obj.account_bal -= self.amount
                debitor_acc_obj.save()
                resp["id"] = transaction_id
                resp["from"] = {
                    "id": self.debit_account_no,
                    "balance": debitor_acc_obj.account_bal
                }
                resp["to"] = {
                    "id": self.credit_account_no,
                    "balance": creditor_acc_obj.account_bal
                }
                resp["transfered"] = self.amount,
                resp["created_datetime"] = transaction_time.strftime("%m/%d/%Y, %H:%M:%S")
                return resp
        except InsufficientBalanceException as err:
            raise err
        except Exception as err:
            print(err)
            Exception(f'{TransactAPIErrorCode.INTERNAL_API_ERROR.value} with error: {err}')
