import enum

AccountNumberLength = 12
AccountNumberPrefix = 10
DEFAULT_TRANSACTION_API_TTL = 10


class TransactAPIErrorCode(enum.Enum):
    INVALID_CREDITOR_ACCOUNT = 'The to account number is invalid'
    INVALID_DEBITOR_ACCOUNT = 'The from account number is invalid'
    NEGATIVE_AMOUNT = 'Amount cannot be negative'
    INSUFFICIENT_INFO = 'Request body is malformed'
    INVALID_REQUEST = 'The request is invalid'
    INSUFFIENT_BALANCE = 'Insufficent Balance in Account'
    INTERNAL_API_ERROR = 'Internal API Error'
    MULTIPLE_TRY_IN_SHORT_INTERVAL = 'The same information came in request in a short time! Try again Later'


