class InvalidRequestException(Exception):
    pass

class InsufficientBalanceException(Exception):
    pass