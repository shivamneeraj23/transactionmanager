from bank.models import UserAccount, Transactions
import uuid
from datetime import datetime
from django.db import transaction


class CreditService:
    def __init__(self, account_no, amount):
        self.account_no = account_no
        self.amount = float(amount)

    def perform_credit(self):
        with transaction.atomic():
            resp = {'success': False}
            try:
                creditor_acc_obj = UserAccount.objects.select_for_update().get(account_no=self.account_no)
                transaction_time = datetime.now()
                creditor_acc_obj.account_bal += self.amount
                creditor_acc_obj.save()
                transaction_id = str(uuid.uuid4())
                Transactions.objects.create(
                    txn_type=Transactions.TXN_TYPE_CHOICES[0][0],
                    txn_id=transaction_id,
                    txn_amount=self.amount,
                    txn_acc=creditor_acc_obj,
                    txn_time=transaction_time
                )
                resp['success'] = True
                return resp
            except Exception as err:
                raise err
