from django.contrib import admin
from django.forms import ModelForm
from .models import UserAccount, Transactions


# Register your models here.

@admin.register(UserAccount)
class UserAccountAdmin(admin.ModelAdmin):
    readonly_fields = ('account_no',)

@admin.register(Transactions)
class TransactionAdmin(admin.ModelAdmin):
    readonly_fields = ('txn_type', 'txn_id', 'txn_amount', 'txn_acc', 'txn_time')
    def has_add_permission(self, request):
        return False