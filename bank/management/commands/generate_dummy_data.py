from django.core.management.base import BaseCommand

from bank.transaction_helper.dummy_data_generator import generate_dummy_data


class Command(BaseCommand):
    help = 'Generate Dummy Data for transactionManager'

    def handle(self, *args, **options):
        try:
            generate_dummy_data()
            self.stdout.write(self.style.SUCCESS('Successfully Created Dummy Data'))
        except Exception as err:
            self.stdout.write(self.style.ERROR('Not sucess'))

