from django.db import models
from django.conf import settings
import math
from datetime import datetime

from bank.transaction_helper.bank_conf import AccountNumberLength, AccountNumberPrefix


class UserAccount(models.Model):
    """
    Model for BankAccounts Present in the System
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    account_no = models.CharField(unique=True, db_index=True, max_length=20, blank=True, null=True)
    account_bal = models.FloatField(null=False, default=0.0)
    is_active = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True, db_index=True)

    def generate_account_no(self, length, pref):
        """
        Account Number to be generated only when moderators mark the account as active.
        Account number is a unique string consisting of the pk of UserAccount.
        :param length: Lenght of the Account Number
        :param pref: Prefix to be added infront of account number
        :return:
        """
        return str(int(self.id) + int((int(pref) * math.pow(10, length))))

    def self_balance_add(self):
        # create a transaction entry.
        pass

    def __str__(self):
        return f'{self.account_no}'

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.account_no and self.id:
            self.account_no = self.generate_account_no(AccountNumberLength, AccountNumberPrefix)
        super().save()


class Transactions(models.Model):
    """
    Model for recording all transactions performed
    """
    TXN_TYPE_CHOICES = (
        ('CR','CREDIT'),
        ('DE', 'DEBIT')
    )
    txn_type = models.CharField(choices=TXN_TYPE_CHOICES, null=False, blank=False, max_length=2)
    txn_id = models.CharField(max_length=40, null=False)
    txn_amount = models.FloatField(null=False)
    txn_acc = models.ForeignKey(UserAccount, on_delete=models.CASCADE)
    txn_time = models.DateTimeField(default=datetime.now())

    def __str__(self):
        return f'{self.txn_id}_{self.txn_type}'
