# Generated by Django 4.1.7 on 2023-02-23 04:48

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bank', '0010_alter_transactions_txn_time'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transactions',
            name='txn_time',
            field=models.DateTimeField(default=datetime.datetime(2023, 2, 23, 10, 18, 38, 609838)),
        ),
    ]
