from django.http import JsonResponse
from http import HTTPStatus
from django.views import View
import uuid
import logging
import json
import traceback

from bank.transaction_helper.credit_manager import CreditService
from bank.transaction_helper.custom_exceptions import InvalidRequestException
from bank.transaction_helper.transaction_manager import TransactService

logger = logging.getLogger('django')


# Create your views here.
def health_check(request):
    return JsonResponse({'health_check': 'OK'}, status=HTTPStatus.OK)


class TransactionManagerView(View):
    def post(self, request):
        resp_payload = {
            'data': [],
            'error': False
        }
        try:
            request_body = json.loads(request.body)
            ts = TransactService(request_body.get('to_account'),
                                 request_body.get('from_account'),
                                 request_body.get('amount'))
            resp_payload['data'] = ts.perform_transaction()
            return JsonResponse(resp_payload, status=HTTPStatus.OK)
        except InvalidRequestException as err:
            resp_payload['error'] = str(err)
            logger.error(f'{traceback.print_exc()}, {str(err)}')
            return JsonResponse(resp_payload, status=HTTPStatus.BAD_REQUEST)
        except Exception as err:
            resp_payload['error'] = str(err)
            logger.error(f'{traceback.print_exc()}, {str(err)}')
            return JsonResponse(resp_payload, status=HTTPStatus.INTERNAL_SERVER_ERROR)
        finally:
            logger.info(f'request_body: {request_body}, '
                        f'response body: {resp_payload}')


class CreditManagerView(View):
    """
    Helper View to add Balance in user account
    """

    def post(self, request):
        try:
            request_body = json.loads(request.body)
            cs = CreditService(
                request_body.get('account_no'),
                request_body.get('amount')
            )
            resp = cs.perform_credit()
            return JsonResponse(resp, status=HTTPStatus.OK)
        except Exception as err:
            return JsonResponse({'error': str(err)}, status=HTTPStatus.INTERNAL_SERVER_ERROR)
