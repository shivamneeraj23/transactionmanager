# TRANSACTION MANAGER

### PREREQUISITES
* docker

### Steps to up the application

1. [ ] `sudo docker-compose up --build -d`

### Steps to Create Superuser and access admin

1. `sudo docker-compose exec web python manage.py createsuperuser`
2. Access Admin through http://localhost:9000/admin/

#### To Generate Dummy Data
1. [ ]  `sudo docker-compose exec web python manage.py generate_dummy_data
`

### POSTMAN API COLLECTION
https://api.postman.com/collections/7869965-69bc91b7-f46d-4d8e-998c-ccc5fa973819?access_key=PMAT-01GSY9S1W0XZRW1913Z7H3YVD2

### Additional Commands
1. `sudo docker-compose exec web python manage.py collectstatic 
`
2. `sudo docker-compose exec web python manage.py createsuperuser 
`
3. `sudo docker-compose exec web python manage.py makemigrations 
`
4. `sudo docker-compose exec web python manage.py migrate 
`

### Salient Design Choices
1. In case of transfer we make two entries in the Transaction Table one for the CREDIT and another for DEBIT with the same transaction number. This is done in order to generate the ledger easily for every account. 
2. UserAccount is an entity which stores all the registered account number present in the banking system,


### IMP Design Resources
1. https://medium.com/@alexandre.laplante/djangos-select-for-update-with-examples-and-tests-caff09414766
2. https://stackoverflow.com/questions/17159471/how-to-use-select-for-update-to-get-an-object-in-django 
